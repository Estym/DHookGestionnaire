import yaml
import io
import os.path
from dhooks import Webhook

setwhile = 1
while setwhile == 1:
    cmd = input('COMMANDE: ')
    if cmd == 'install':
        name = input('FileName: ' )
        server = input('ServerName: ' )
        hook = input('HookLink: ')
        f = open('hooks/' + name + '.yml','w')
        data = {'hook': {'name': name, 'server': server, 'hooklink': hook}}
        with io.open('hooks/' + name +'.yml', 'w', encoding='utf8') as outfile:
            yaml.dump(data, outfile, default_flow_style=False, allow_unicode=True)
            f.close()
            print('Fichier enregistré')
    else:
        if cmd == 'info':
            arg = input('Nom du Hook: ')
            path = 'hooks/' + arg +'.yml'
            if os.path.exists(path):
                with open(path, 'r') as stream:
                    data_loaded = yaml.load(stream, Loader=yaml.FullLoader)
                    dic=data_loaded
                    hook = dic['hook']['hooklink']
                    name = dic['hook']['name']
                    server = dic['hook']['server']
                    print()
                    print('Infos de ' + arg +':')
                    print('NAME= ' + name)
                    print('SERVER= ' + server)
                    print('HOOK= ' + hook)
                    print()
            else:
                print('Fichier inconnu')
        else:
            if cmd == 'send':
                arg = input('Nom du hook: ')
                path = 'hooks/' + arg + '.yml'
                if os.path.exists(path):
                    with open(path, 'r') as stream:
                        data_loaded = yaml.load(stream, Loader=yaml.FullLoader)
                        dic=data_loaded
                        link = dic['hook']['hooklink']
                        server = dic['hook']['server']
                        print('Envoyer un message à: ' + server)
                        msg = input('Message: ')
                        hook = Webhook(link)
                        hook.send(msg)
                        print('Message envoyé à ' + server + ' !')
                else:
                    print('Fichier inconnu')
            else:
                if cmd == 'close':
                    setwhile = 0
                else:
                    if cmd == 'remove':
                        arg = input('Nom du hook: ')
                        path = 'hooks/' + arg + '.yml'
                        if os.path.exists(path):
                            os.remove(path)
                            print(arg + ' supprimé !')
                        else:
                            print('Ce hook est inconnu :(')
                    else:
                        if cmd == 'list':
                            hooklist = os.listdir('hooks')
                            print('Liste des hooks:')
                            print(hooklist)
                        else:
                            if cmd == 'commandes':
                                print()
                                print('Liste des commandes:')
                                print('install: Ajouter un nouveau WebHook')
                                print('info: Voir les informations sur un WebHook')
                                print('send: Envoie un message via un WebHook')
                                print('list: Voir la liste des WebHook')
                                print('commandes: Affiche ce message')
                                print('close: Ferme le programme')
                                print()
                            else:
                                print('Commande inconnue, faites: commandes')
