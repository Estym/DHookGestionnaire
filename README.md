# DHookGestionnaire
DHookGestionnaire est un programme rapide de gestion et d'envoi de message via les magnifiques WebHooks discord.
Pour ajouter un WebHook, créez le sur votre panel d'administration de votre serveur Discord et copiez le.
allez sur le programme (main.py) et lancez le (Note: Il faut installer les modules dhooks et pyyaml via le pip) après celà
tapez `install` pour créer un nouveau WebHook, suivez les etapes puis faites `send` choisissez le WebHook que vous avez
installé, et entrez le message que vous souhaitez envoyer. Et voila, le WebHook est fonctionnel :)
